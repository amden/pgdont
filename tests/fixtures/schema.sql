-- UpperCaseTableRueTestCase
CREATE TABLE "TestTableName" (
    id SERIAL PRIMARY KEY
);
CREATE TABLE "SecondTestTableName" (
    id SERIAL PRIMARY KEY
);
CREATE TABLE "Second_Test_TableName" (
    id SERIAL PRIMARY KEY
);


-- UpperCaseColumnTestCase
CREATE TABLE "testtable" (
    id SERIAL PRIMARY KEY,
    "Title" VARCHAR
);


-- RulesRuleTestCase
CREATE TABLE "testrule" (
    id SERIAL PRIMARY KEY,
    title VARCHAR
);
CREATE RULE test_rule AS ON UPDATE 
    TO testrule
    DO UPDATE testrule SET title='title'
    WHERE id = NEW.id; 


-- InheritanceRuleTestCase
CREATE TABLE parent (
    parent_name VARCHAR
);
CREATE TABLE child (
    title VARCHAR
) INHERITS (parent);

-- DateTimeRulesTestCase 
CREATE TABLE testdatetime (
    timestamp_no_tz TIMESTAMP,
    timestamp_no_tz_2 TIMESTAMP WITHOUT TIME ZONE,
    timestamp_tz TIMESTAMP WITH TIME ZONE,
    time_tz TIME WITH TIME ZONE --TIMETZ
);
